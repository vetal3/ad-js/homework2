const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

function listCreation(books) {
  const root = document.getElementById("root");
  const ul = document.createElement("ul");
  
  books.forEach(book => {
    if (typeof book.author === "undefined" || typeof book.name === "undefined" || typeof book.price === "undefined") {
      console.error(`Помилка: в об'єкті відсутня одна з властивостей: ${JSON.stringify(book)}`);
      return;
    }

    const li = document.createElement("li");

    li.innerHTML = `Автор: ${book.author}<br>Назва: ${book.name}<br>Ціна: ${book.price}`;
    ul.append(li);
  });

  root.append(ul);
}

try {
  listCreation(books);
} catch (error) {
  console.log(`Ошибка  ${error.name}: ${error.message}`);
}